# Cobalt Registry
This Git repo contains the registry of customers, projects, metrics and
reports for the [Cobalt](https://fuchsia.googlesource.com/cobalt/)
telemetry system.

## Background
Cobalt is a system for telemetry with built-in privacy. It is a pipeline
for collecting metrics data from user-owned devices in the field and
producing aggregate reports. Cobalt includes a suite of features for preserving
user privacy and anonymity while giving product owners the data they need
to improve their products. A software or hardware developer would use
Cobalt to learn about the behavior of their products in the field,
in order to make them better.

In order to use Cobalt, a developer must register a *project*. Within
the project a developer must register one or more *metrics*. Within
a metric a developer must register one or more *reports*. These registrations
all involve entries into .yaml files in this repository.

## Instructions
You will need a project, one or more metrics, and one or more reports.

### Register a project
If you are part of a team that is already using Cobalt you likely can use
your team's existing project. Projects are grouped into *Customers*. A
customer represents a larger organization consisting of many teams. For
example, as of this writing, *fuchsia* is a customer representing the
group of people and projects involved in developing the Fuchsia operating
system. Within the Fuchsia customer there are several projects such
as *connectivity_wlan* and *local_storage*.

The list of customers and projects is in
[projects.yaml](https://fuchsia.googlesource.com/cobalt-registry/+/refs/heads/main/projects.yaml).
Add a new
customer or project to that file as necessary.

Associated with each customer there is a directory in this repo and associated
with each project there is a directory below the parent customer directory.
For example associated with the *connectivity_wlan* project within the
*fuchsia* customer is the directory
[fuchsia/connectivity_wlan](https://fuchsia.googlesource.com/cobalt-registry/+/refs/heads/main/fuchsia/connectivity_wlan/).
If you create a new project you must also create a new project directory of the
same name.

### Register a metric
Within your project's directory (see **Register a project** above) there must
be a file named `metrics.yaml`. This is where you register your metrics and
reports. If you just created a new project you should now create `metrics.yaml`.
In this case you can copy one of the `metrics.yaml` files from one of the
other projects to use as a template.

Inside of `metrics.yaml` there is a field called `metric_definitions` that
contains a list of metric definitions. You must add a new entry for your new metric. You
must give your new metric a name distinct from the names of the other
metrics in the file and you must assign it the next available integer
ID.

### Register a report
The `reports` field within a metric definition contains the list of registered reports associated with the metric. You do not
need to register any reports in order to start logging data for the new metric. But Cobalt will not send any data to the
server nor generate any reports for the metric until you do.

### Yaml to Proto
Under the hood, Cobalt will translate the data in `metrics.yaml` into a
protocol buffer message. It is useful to know this because then you can use
the definition of the proto messages as the specification for the corresponding
fields in the yaml file. Each metric definition entry will be translated
into a `MetricDefinition` proto message as defined in
[metric_definition.proto](https://fuchsia.googlesource.com/cobalt/+/refs/heads/main/src/registry/metric_definition.proto) and each report definition entry will be
translated into a `ReportDefinition` proto message as defined in
[report_definition.proto](https://fuchsia.googlesource.com/cobalt/+/refs/heads/main/src/registry/report_definition.proto). This translation is accomplished at build
time by Cobalt's
[registry parser](https://fuchsia.googlesource.com/cobalt/+/refs/heads/main/src/bin/config_parser/src/).

