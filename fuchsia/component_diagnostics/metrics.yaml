# Copyright 2020 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

metric_definitions:
################################################################################
# GetNext Requests
#
# The number of times that a client of Archivist reads a new batch.
#
# Every minute we log the number of times that a client of Archivist
# has read a new batch, along with event codes specifying which pipeline
# was read and the data type of the batch read.
#
################################################################################
- id: 101
  metric_name: batch_iterator_get_next_requests_migrated
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING]
  metric_dimensions:
    - dimension: pipeline
      event_codes: &pipelines
        0: All
        1: Feedback
        2: LegacyMetrics
        3: LoWPAN
    - dimension: data_type
      event_codes: &data_types
        0: Inspect
        1: Logs
        2: Lifecycle
  reports:
    - report_name: batch_iterator_get_next_requests
      id: 1
      report_type: HOURLY_VALUE_NUMERIC_STATS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, ARCH, SYSTEM_VERSION, OS, CHANNEL]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/07/01"
################################################################################
# GetNext Result Count
#
# The number of results that were returned from GetNext calls
#
# Every minute we log the number of results Archivist returns in GetNext calls
# along with event codes specifying which pipeline and data type was returned.
#
################################################################################
- id: 103
  metric_name: batch_iterator_get_next_result_count_migrated
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING]
  metric_dimensions:
    - dimension: pipeline
      event_codes: *pipelines
    - dimension: data_type
      event_codes: *data_types
  reports:
    - report_name: batch_iterator_get_next_result_count
      id: 1
      report_type: HOURLY_VALUE_NUMERIC_STATS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, ARCH, SYSTEM_VERSION, OS, CHANNEL]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/07/01"
################################################################################
# GetNext Result Errors
#
# The number of results containing errors returned from GetNext
#
# Every minute we log the number of error results Archivist returns in GetNext calls
# along with event codes specifying which pipeline and data type was returned.
#
################################################################################
- id: 104
  metric_name: batch_iterator_get_next_result_errors_migrated
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING]
  metric_dimensions:
    - dimension: pipeline
      event_codes: *pipelines
    - dimension: data_type
      event_codes: *data_types
  reports:
    - report_name: batch_iterator_get_next_result_errors
      id: 1
      report_type: HOURLY_VALUE_NUMERIC_STATS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, ARCH, SYSTEM_VERSION, OS, CHANNEL]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/07/01"
################################################################################
# Component timeouts count
#
# The number of components that timed out while reading their data.
#
# Every minute we log the number of times the Archivist attempted to read data
# for a component, but that component timed out, along with event codes
# specifying which pipeline and data type Archivist was attempting to read.
#
################################################################################
- id: 105
  metric_name: component_timeouts_count_migrated
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING]
  metric_dimensions:
    - dimension: pipeline
      event_codes: *pipelines
    - dimension: data_type
      event_codes: *data_types
  reports:
    - report_name: component_timeouts_count
      id: 1
      report_type: HOURLY_VALUE_NUMERIC_STATS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, ARCH, SYSTEM_VERSION, OS, CHANNEL]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/07/01"
################################################################################
# Archive Accessor Connections
#
# The number of connections opened to the ArchiveAccessor
#
# Every minute we log the number of incoming connections to the Archivist,
# along with an event code specifying which pipeline was connected to.
#
################################################################################
- id: 106
  metric_name: archive_accessor_connections_opened_migrated
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING]
  metric_dimensions:
    - dimension: pipeline
      event_codes: *pipelines
  reports:
    - report_name: archive_accessor_connections_opened
      id: 1
      report_type: HOURLY_VALUE_NUMERIC_STATS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, ARCH, SYSTEM_VERSION, OS, CHANNEL]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/07/01"
################################################################################
# Schema Truncation Count
#
# The number of schemas the diagnostics platform truncated to avoid exceeding
# size budget.
#
# Every minute we log a uint counter that increments each time a schema is truncated.
#
################################################################################
- id: 108
  metric_name: schema_truncation_count_migrated
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING]
  metric_dimensions:
    - dimension: pipeline
      event_codes: *pipelines
    - dimension: data_type
      event_codes: *data_types
  reports:
    - report_name: schema_truncation_count
      id: 1
      report_type: HOURLY_VALUE_NUMERIC_STATS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, ARCH, SYSTEM_VERSION, OS, CHANNEL]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/07/01"

# Deleted metric IDs that must not be reused.
deleted_metric_ids: [1,2,3,4,5,6,7,8,102,107]
