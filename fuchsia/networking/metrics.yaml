# Copyright 2019 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

metric_definitions:

###############################################################################
# fuchsia networking
#
###############################################################################

###############################################################################
# socket_count_max
#
# Every minute we log the maximum number of open sockets observed on the system
# during the previous minute-long period.
###############################################################################
- id: 15
  metric_name: socket_count_max
  metric_type: INTEGER
  metric_units_other: "sockets"
  metric_semantics: [USAGE_COUNTING, NETWORK_COMMUNICATION]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/03/01"
  reports:
    - report_name: socket_count_max
      id: 1
      report_type: FLEETWIDE_HISTOGRAMS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 8
          initial_step: 25
          step_multiplier: 2
    - report_name: socket_count_per_device_max
      id: 2
      report_type: UNIQUE_DEVICE_HISTOGRAMS
      system_profile_selection: SELECT_LAST
      local_aggregation_period: WINDOW_1_DAY
      local_aggregation_procedure: MAX_PROCEDURE
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 8
          initial_step: 25
          step_multiplier: 2

###############################################################################
# sockets_created
#
# Every minute we log the number of sockets created in the last minute.
###############################################################################
- id: 28
  metric_name: sockets_created
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING, NETWORK_COMMUNICATION]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/03/01"
  reports:
    - report_name: sockets_created
      id: 1
      report_type: FLEETWIDE_OCCURRENCE_COUNTS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 8
          initial_step: 25
          step_multiplier: 2
    - report_name: sockets_created_per_device_max
      id: 2
      report_type: UNIQUE_DEVICE_HISTOGRAMS
      system_profile_selection: SELECT_LAST
      local_aggregation_period: WINDOW_1_DAY
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 8
          initial_step: 25
          step_multiplier: 2

################################################################################
# sockets_destroyed
#
# Every minute we log the number of sockets destroyed in the last minute.
################################################################################
- id: 29
  metric_name: sockets_destroyed
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING, NETWORK_COMMUNICATION]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/03/01"
  reports:
    - report_name: sockets_destroyed
      id: 1
      report_type: FLEETWIDE_OCCURRENCE_COUNTS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 8
          initial_step: 25
          step_multiplier: 2
    - report_name: sockets_destroyed_per_device_max
      id: 2
      report_type: UNIQUE_DEVICE_HISTOGRAMS
      system_profile_selection: SELECT_LAST
      local_aggregation_period: WINDOW_1_DAY
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 8
          initial_step: 25
          step_multiplier: 2

################################################################################
# packets_sent
#
# Every minute we log the number of packets sent in the last minute.
################################################################################
- id: 30
  metric_name: packets_sent
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING, NETWORK_COMMUNICATION]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/03/01"
  reports:
    - report_name: packets_sent
      id: 1
      report_type: UNIQUE_DEVICE_HISTOGRAMS
      system_profile_selection: SELECT_LAST
      local_aggregation_period: WINDOW_1_DAY
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 16
          initial_step: 10
          step_multiplier: 2

################################################################################
# packets_received
#
# Every minute we log the number of packets received in the last period.
################################################################################
- id: 31
  metric_name: packets_received
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING, NETWORK_COMMUNICATION]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/03/01"
  reports:
    - report_name: packets_received
      id: 1
      report_type: UNIQUE_DEVICE_HISTOGRAMS
      system_profile_selection: SELECT_LAST
      local_aggregation_period: WINDOW_1_DAY
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 16
          initial_step: 10
          step_multiplier: 2

################################################################################
# bytes_sent
#
# Every minute we log the number of bytes sent in the last period.
################################################################################
- id: 32
  metric_name: bytes_sent
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING, NETWORK_COMMUNICATION]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/03/01"
  reports:
    - report_name: bytes_sent
      id: 1
      report_type: UNIQUE_DEVICE_HISTOGRAMS
      system_profile_selection: SELECT_LAST
      local_aggregation_period: WINDOW_1_DAY
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 16
          initial_step: 100
          step_multiplier: 10

################################################################################
# bytes_received
#
# Every minute we log the number of bytes received in the last period.
################################################################################
- id: 33
  metric_name: bytes_received
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING, NETWORK_COMMUNICATION]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/03/01"
  reports:
    - report_name: bytes_received
      id: 1
      report_type: UNIQUE_DEVICE_HISTOGRAMS
      system_profile_selection: SELECT_LAST
      local_aggregation_period: WINDOW_1_DAY
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 16
          initial_step: 10
          step_multiplier: 2

################################################################################
# tcp_connections_established_total
#
# Every minute we log the number of TCP connections established at the end of
# the last period.
#
# Such a metric, when viewed over time, would help us understand the number of
# TCP connections that are building up in the system. A build-up can occur
# due to many long-lived sessions and/or TCP connections waiting to be
# purged by the stack. Such a wait occurs on passive close, waiting for
# application to initiate a close and on active close, waiting for certain
# timeouts to trigger.
#
# This is different from the socket_count metric above as the socket close
# and destruction triggered by the application is independent of the time TCP
# connections actually linger in the stack.
################################################################################
- id: 22
  metric_name: tcp_connections_established_total
  metric_type: INTEGER
  metric_units_other: "tcp connections"
  metric_semantics: [USAGE_COUNTING, NETWORK_COMMUNICATION]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/03/01"
  reports:
    - report_name: tcp_connections_established_total
      id: 1
      report_type: FLEETWIDE_HISTOGRAMS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 8
          initial_step: 25
          step_multiplier: 2
    - report_name: tcp_connections_established_total_per_device_max
      id: 2
      report_type: UNIQUE_DEVICE_HISTOGRAMS
      system_profile_selection: SELECT_LAST
      local_aggregation_period: WINDOW_1_DAY
      local_aggregation_procedure: MAX_PROCEDURE
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 8
          initial_step: 25
          step_multiplier: 2

################################################################################
# tcp_connections_closed
#
# Every minute we log the number of established TCP connections that got closed
# because of passive or active TCP close by applications in the last minute.
################################################################################
- id: 34
  metric_name: tcp_connections_closed
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING, NETWORK_COMMUNICATION]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/03/01"
  reports:
    - report_name: tcp_connections_closed
      id: 1
      report_type: FLEETWIDE_OCCURRENCE_COUNTS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 8
          initial_step: 25
          step_multiplier: 2
    - report_name: tcp_connections_closed_per_device_max
      id: 2
      report_type: UNIQUE_DEVICE_HISTOGRAMS
      system_profile_selection: SELECT_LAST
      local_aggregation_period: WINDOW_1_DAY
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 8
          initial_step: 25
          step_multiplier: 2

################################################################################
# tcp_connections_reset
#
# Every minute we log the number of established TCP connections that got aborted
# whenever a TCP RST was sent/received by the stack in the last period.
################################################################################
- id: 35
  metric_name: tcp_connections_reset
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING, NETWORK_COMMUNICATION]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/03/01"
  reports:
    - report_name: tcp_connections_reset
      id: 1
      report_type: FLEETWIDE_OCCURRENCE_COUNTS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 8
          initial_step: 25
          step_multiplier: 2
    - report_name: tcp_connections_reset_per_device_max
      id: 2
      report_type: UNIQUE_DEVICE_HISTOGRAMS
      system_profile_selection: SELECT_LAST
      local_aggregation_period: WINDOW_1_DAY
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 8
          initial_step: 25
          step_multiplier: 2

################################################################################
# tcp_connections_timed_out
#
# Every minute we log the number of established TCP connections that got closed
# because of TCP keepalive timeout expiry in the last period.
################################################################################
- id: 36
  metric_name: tcp_connections_timed_out
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING, NETWORK_COMMUNICATION]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/03/01"
  reports:
    - report_name: tcp_connections_timed_out
      id: 1
      report_type: FLEETWIDE_OCCURRENCE_COUNTS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 8
          initial_step: 25
          step_multiplier: 2
    - report_name: tcp_connections_timed_out_per_device_max
      id: 2
      report_type: UNIQUE_DEVICE_HISTOGRAMS
      system_profile_selection: SELECT_LAST
      local_aggregation_period: WINDOW_1_DAY
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 8
          initial_step: 25
          step_multiplier: 2

###############################################################################
# dhcp_v6_configuration
#
# Every minute, we log the DHCPv6 configurations we've observed in the last
# minute.
###############################################################################
- id: 26
  metric_name: dhcp_v6_configuration
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING, NETWORK_COMMUNICATION]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/03/01"
  metric_dimensions:
    - dimension: configuration_from_ndpra
      event_codes:
        0: no_configuration
        1: managed_address
        2: other_configurations
      max_event_code: 7
  reports:
    - report_name: dhcp_v6_configuration
      id: 1
      report_type: UNIQUE_DEVICE_COUNTS
      system_profile_selection: SELECT_LAST
      local_aggregation_period: WINDOW_1_DAY
      local_aggregation_procedure: AT_LEAST_ONCE
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
    - report_name: dhcp_v6_configuration_7_day_actives
      id: 2
      report_type: UNIQUE_DEVICE_COUNTS
      system_profile_selection: SELECT_LAST
      local_aggregation_period: WINDOW_7_DAYS
      local_aggregation_procedure: AT_LEAST_ONCE
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]

###############################################################################
# available_dynamic_ipv6_address_config
#
# Every hour, logs whether IPv6 addresses are available via DHCPv6 and SLAAC.
###############################################################################
- id: 37
  metric_name: available_dynamic_ipv6_address_config
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING, NETWORK_COMMUNICATION]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/03/01"
  metric_dimensions:
    - dimension: dynamic_ipv6_address_source
      event_codes:
        0: no_global_slaac_or_dhcpv6_managed_address
        1: global_slaac_only
        2: dhcpv6_managed_address_only
        3: global_slaac_and_dhcpv6_managed_address
  reports:
    - report_name: available_dynamic_ipv6_address_config
      id: 1
      report_type: UNIQUE_DEVICE_NUMERIC_STATS
      local_aggregation_period: WINDOW_1_DAY
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
    - report_name: available_dynamic_ipv6_address_config_7_day_actives
      id: 2
      report_type: UNIQUE_DEVICE_NUMERIC_STATS
      local_aggregation_period: WINDOW_7_DAYS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]

# Deleted metric IDs that must not be reused.
deleted_metric_ids: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,18,19,20,21,23,24,25,27]
