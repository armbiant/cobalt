# Copyright 2019 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

################################################################################
# Cobalt Project: media
################################################################################

metric_definitions:

#####################################################################
# Next Metric ID: 13
#####################################################################

################################################################################
# audio_session_duration
#
# Total amount of time (ns) in which audio input or output was being processed.
# Typically this is the time between Play/Pause or Record/Stop, or for components
# behind a mixer, it's the time between the first Play of any input being mixed
# until all inputs have been Stopped.
################################################################################
- id: 103
  metric_name: audio_session_duration_migrated
  metric_type: INTEGER
  metric_semantics: [LATENCY]
  metric_units_other: "nanoseconds"

  metric_dimensions:
    - dimension: component
      event_codes: &component_event_codes
        0: OutputDevice
        1: OutputPipeline
        2: Renderer
        3: Capturer

  reports:
    - report_name: audio_session_duration_stats
      id: 1
      report_type: FLEETWIDE_MEANS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
    - report_name: audio_session_duration_histograms
      id: 2
      report_type: FLEETWIDE_HISTOGRAMS
      int_buckets: &nanosecond_duration_buckets_one_day
        exponential:
          # The effective range is 1ms to 18hr, which allows us to represent sessions
          # up to about 1 day long. The floor of the largest bucket is 1ms*2^26, or
          # 67108s, or about 18h.
          floor: 0
          num_buckets: 27
          initial_step: 1000000
          step_multiplier: 2
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]

  meta_data:
    max_release_stage: GA
    expiration_date: "2021/08/11"

################################################################################
# audio_overflow_duration
#
# Total amount of time (ns) in which the system was stalled by an overflow.
################################################################################
- id: 104
  metric_name: audio_overflow_duration_migrated
  metric_type: INTEGER
  metric_semantics: [LATENCY]
  metric_units_other: "nanoseconds"
  metric_dimensions:
    - dimension: component
      event_codes: *component_event_codes
  reports:
    - report_name: audio_overflow_duration_stats
      id: 1
      report_type: FLEETWIDE_MEANS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
    - report_name: audio_overflow_duration_histograms
      id: 2
      report_type: FLEETWIDE_HISTOGRAMS
      int_buckets: &nanosecond_duration_buckets_ten_minutes
        exponential:
          # The effective range is 1ms to 10min. We expect that overflows and underflows
          # are typically at most a few seconds long, so this is plenty of precision.
          # The floor of the largest bucket is 1ms*2^19, or 524s, or about 8min.
          floor: 0
          num_buckets: 20
          initial_step: 1000000
          step_multiplier: 2
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]

  meta_data:
    max_release_stage: GA
    expiration_date: "2021/08/11"

################################################################################
# audio_underflow_duration
#
# Total amount of time (ns) in which the system was stalled by an underflow.
################################################################################
- id: 105
  metric_name: audio_underflow_duration_migrated
  metric_type: INTEGER
  metric_semantics: [LATENCY]
  metric_units_other: "nanoseconds"
  metric_dimensions:
    - dimension: component
      event_codes: *component_event_codes
  reports:
    - report_name: audio_underflow_duration_stats
      id: 1
      report_type: FLEETWIDE_MEANS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
    - report_name: audio_underflow_duration_histograms
      id: 2
      report_type: FLEETWIDE_HISTOGRAMS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
      int_buckets: *nanosecond_duration_buckets_ten_minutes

  meta_data:
    max_release_stage: GA
    expiration_date: "2021/08/11"

################################################################################
# audio_time_since_last_overflow_or_session_start
#
# Total amount of time (ns) since the last overflow event, or since the session
# started, whichever happened most recently. This is recorded at every overflow
# event and when the session ends.
################################################################################
- id: 106
  metric_name: audio_time_since_last_overflow_or_session_start_migrated
  metric_type: INTEGER
  metric_semantics: [LATENCY]
  metric_units_other: "nanoseconds"
  metric_dimensions:
    - dimension: component
      event_codes: *component_event_codes

    - dimension: last_event
      event_codes:
        0: SessionStart
        1: Overflow
  reports:
    - report_name: audio_time_since_last_overflow_or_session_start_histograms
      id: 2
      report_type: FLEETWIDE_HISTOGRAMS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
      int_buckets: *nanosecond_duration_buckets_one_day

  # Deleted report IDs that must not be reused.
  deleted_report_ids: [1]

  meta_data:
    max_release_stage: GA
    expiration_date: "2021/08/11"

################################################################################
# audio_time_since_last_underflow_or_session_start
#
# Total amount of time (ns) since the last underflow event, or since the session
# started, whichever happened most recently. This is recorded at every underflow
# event and when the session ends.
################################################################################
- id: 107
  metric_name: audio_time_since_last_underflow_or_session_start_migrated
  metric_type: INTEGER
  metric_semantics: [LATENCY]
  metric_units_other: "nanoseconds"
  metric_dimensions:
    - dimension: component
      event_codes: *component_event_codes

    - dimension: last_event
      event_codes:
        0: SessionStart
        1: Underflow
  reports:
    - report_name: audio_time_since_last_overflow_or_session_start_histograms
      id: 2
      report_type: FLEETWIDE_HISTOGRAMS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
      int_buckets: *nanosecond_duration_buckets_one_day

  # Deleted report IDs that must not be reused.
  deleted_report_ids: [1]

  meta_data:
    max_release_stage: GA
    expiration_date: "2021/08/11"

################################################################################
# audio_objects_created
#
# Count of audio renderers created, broken down by type and format.
################################################################################
- id: 110
  metric_name: audio_objects_created_migrated
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING]
  metric_dimensions:
    - dimension: object_type
      event_codes:
        0: OutputDevice
        1: InputDevice
        2: Renderer
        3: Capturer
    - dimension: sample_format
      event_codes:
        0: Other
        1: Uint8
        2: Int16
        3: Int24
        4: Float32
    - dimension: channels
      event_codes:
        0: Other
        1: Chan1
        2: Chan2
        3: Chan4
    - dimension: frame_rate
      event_codes:
        0: Other
        1: Rate16000
        2: Rate44100
        3: Rate48000
        4: Rate96000
  reports:
    - report_name: audio_objects_created_stats
      id: 1
      report_type: HOURLY_VALUE_NUMERIC_STATS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]

  meta_data:
    max_release_stage: GA
    expiration_date: "2021/10/11"

################################################################################
# stream_processor_events_2
#
# We log counts of various stream processor events.  We use an EVENT_COUNT to
# allow accumulating event counts in-process before sending the count to cobalt.
#
# Expected frequency: Batch logging frequency capped at no more than once every
# 5 seconds.  The max batch size of 64 will essentially always be large enough
# to avoid spilling into an additional batch.  Batch sizes larger than 12 are
# expected to be rare, as 12 is the number of different events we'd expect to
# see if streams are being created and destroyed successfully, plus one type of
# generic error and one more specific error.  It's rare that we'd see a storm of
# different error codes within 5 seconds.
################################################################################
- id: 109
  metric_name: stream_processor_events_2_migrated
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING]
  metric_dimensions:
    - dimension: implementation
      event_codes:
        # Unspecified is used in a few places where we don't actually need
        # metrics, to avoid going over 1023 potential combinations in Cobalt v1.
        #
        # In Cobalt v2, the need for this usage of Unspecified will go away
        # becuase only combinations which actually occur on a given device in
        # 24h will count toward the limit.
        0: Unspecified
        1: AmlogicDecoderShared
        2: AmlogicDecoderH264
        3: AmlogicDecoderVp9
    - dimension: event
      event_codes:
        # Often under a "shared" component since sometimes the hosting process
        # will start before we know which specific stream processor.
        0: HostProcessStart
        # In addition to separate reasons listed below.
        1: StreamProcessorFailureAnyReason
        # In addition to separate reasons listed below.
        2: StreamFailureAnyReason
        3: StreamCreated
        4: StreamDeleted
        5: StreamFlushed
        6: StreamEndOfStreamInput
        7: StreamEndOfStreamOutput
        # In addition to separate reasons listed below.
        8: CoreFailureAnyReason
        9: CoreCreated
        10: CoreDeleted
        11: CoreFlushed
        12: CoreEndOfStreamInput
        13: CoreEndOfStreamOuput
        14: InputBufferAllocationStarted
        # WaitForBuffersAllocated() done (with success or failure)
        15: InputBufferAllocationCompleted
        # WaitForBuffersAllocated() succeeded and codec_impl config succeeded.
        16: InputBufferAllocationSuccess
        # WaitForBuffersAllocated() allocation_status is not ZX_OK.
        17: InputBufferAllocationFailure
        18: OutputBufferAllocationStarted
        # WaitForBuffersAllocated() done (with success or failure)
        19: OutputBufferAllocationCompleted
        # WaitForBuffersAllocated() succeeded and codec_impl config succeeded.
        20: OutputBufferAllocationSuccess
        # WaitForBuffersAllocated() allocation_status is not ZX_OK.
        21: OutputBufferAllocationFailure
        22: InitializationError
        23: AllocationError
        24: FirmwareSizeError
        25: StuckError
        26: FlushError
        27: InterlacedUnsupportedError
        28: ChromaFormatUnsupportedError
        29: MissingPictureError
        30: ZeroMbWidthError
        31: DimensionTooLargeError
        32: CropInfoError
        33: LevelZeroError
        34: LevelTooLargeError
        35: MaxDpbMbsError
        36: RedundantPicCntUnsupportedError
        37: SliceExtensionUnsupportedError
        38: MmcoSrcCmdCountUnsupportedError
        39: MmcoCommandError
        40: ReorderCommandError
        41: MmcoDstCmdCountUnsupportedError
        42: FirstMbInSliceError
        43: BrokenPictureBodyError
        44: GenericDecodeError
        45: MissingOutputSurfaceError
        46: UnreachableError
        47: DecodeResultInvalidError
        48: SwHwSyncError
        49: AspectRatioIdcTooLargeError
        50: NumSliceGroupsUnsupportedError
        51: NumRefIdxDefaultActiveError
        52: PicInitQpRangeError
        53: ReorderListTooLargeError
        54: FrameNumError
        55: PicOrderCntError
        56: TimeoutWaitingForHwError
        57: MotionVectorContextError
        58: HwTimeoutError
        59: StaleInterruptSeen
        60: DrmConfigError
        61: NumRefFramesInPocCycleError
        62: SeqParameterSetIdTooLargeError
        63: InputBufferFullError
        64: InputHwTimeout
        65: StreamReset
        66: MaxFrameNumTooLargeError
        67: InputHwError
        68: InputProcessingError
        69: WatchdogFired
        70: FirmwareLoadError
        71: SlowFrameSkip
        72: DimensionsInvalidError
        73: DimensionsUnsupportedError
        74: EndOfStreamTimeoutError
        75: ClientProtocolError
        76: FidlError
        77: SysmemChannelClosed
        78: ClientConstraintsFailure
        79: MaxInFlightStreamsExceededError
        80: ClientProtocolFailure
  reports:
    - report_name: stream_processor_events_device_histogram
      id: 1
      report_type: UNIQUE_DEVICE_HISTOGRAMS
      system_profile_selection: SELECT_LAST
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 24
          initial_step: 1
          step_multiplier: 2
      local_aggregation_period: WINDOW_1_DAY
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
    - report_name: stream_processor_events_device_stats
      id: 2
      report_type: UNIQUE_DEVICE_NUMERIC_STATS
      local_aggregation_period: WINDOW_1_DAY
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
    - report_name: stream_processor_events_fleet_histogram
      id: 3
      report_type: HOURLY_VALUE_HISTOGRAMS
      system_profile_selection: SELECT_LAST
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 48
          initial_step: 1
          step_multiplier: 2
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
    - report_name: stream_processor_events_fleet_stats
      id: 4
      report_type: HOURLY_VALUE_NUMERIC_STATS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
    # This report is not supported in Cobalt 1.1, as it uses component strings
    # - report_name: stream_processor_events_component_occurrence_count
    #   id: 5
    #   report_type: EVENT_COMPONENT_OCCURRENCE_COUNT
    #   privacy_level: NO_ADDED_PRIVACY
    #   system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
  meta_data:
    max_release_stage: GA
    expiration_date: "2021/08/18"

################################################################################
# audio_thermal_state_duration
#
# Total amount of time (ns) spent in each 'thermal_state' since boot. The
# 'thermal_state' dimension is used to select and identify which state the
# device is in, with "normal" representing the baseline state, and consecutive
# states representing the various thermal configuration states available.
################################################################################
- id: 111
  metric_name: audio_thermal_state_duration_migrated
  metric_type: INTEGER
  metric_semantics: [LATENCY]
  metric_units_other: "nanoseconds"
  metric_dimensions:
    - dimension: thermal_state
      event_codes: &thermal_state_event_codes
        0: normal
        1: state_1
        2: state_2
  reports:
    - report_name: audio_thermal_state_duration_stats
      id: 1
      report_type: FLEETWIDE_MEANS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]

  meta_data:
    max_release_stage: GA
    expiration_date: "2021/12/08"

################################################################################
# audio_thermal_state_transitions
#
# We log the number of thermal state transitions per device. The histogram
# groups devices into buckets, with each bucket corresponding to the count of
# 'state_transitions' to a 'thermal_state'.
################################################################################
- id: 112
  metric_name: audio_thermal_state_transitions_migrated
  metric_type: INTEGER_HISTOGRAM
  metric_semantics: [USAGE_COUNTING]
  metric_units_other: "state transitions"
  metric_dimensions:
    - dimension: state_transition
      event_codes: *thermal_state_event_codes
  int_buckets:
    linear:
      floor: 0
      num_buckets: 50
      step_size: 1
  reports:
    - report_name: audio_thermal_state_transitions_histograms
      id: 1
      report_type: FLEETWIDE_HISTOGRAMS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]

  meta_data:
    max_release_stage: GA
    expiration_date: "2021/12/08"

# Deleted metric IDs that must not be reused.
deleted_metric_ids: [1,2,3,4,5,6,7,8,9,10,11,12]
