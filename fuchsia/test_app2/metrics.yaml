# Copyright 2018 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

metric_definitions:

#####################################################################
# error_occurred_new
#
# This is a fictional metric used for the development of Cobalt 1.1.
# We log each time an error occurs, noting the type of error.
#####################################################################
- id: 12
  metric_name: error_occurred_new
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING]
  metric_dimensions:
    - dimension: status
      event_codes:
        0: OK
        1: InvalidArgument
        2: FailedPrecondition
        9: Unknown
      max_event_code: 9
  reports:
    #####################################################################
    # error_counts
    #
    # This is a fictional report used for the development of Cobalt 1.1.
    # For each day, we count the total number of occurrences over the
    # fleet of each error type.
    #####################################################################
    - report_name: error_counts
      id: 1
      report_type: FLEETWIDE_OCCURRENCE_COUNTS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [OS, ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL, REALM]
    - report_name: error_counts_privacy
      id: 2
      report_type: FLEETWIDE_OCCURRENCE_COUNTS
      privacy_level: LOW_PRIVACY
      # Max is 1 error per second.
      min_value: 0
      max_value: 3600
      system_profile_field: [OS, ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL, REALM]
    - report_name: error_counts_experiment
      id: 3
      report_type: FLEETWIDE_OCCURRENCE_COUNTS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [OS, ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL, REALM, EXPERIMENT_IDS]
      experiment_id: [48954961, 48954962]
    - report_name: error_counts_privacy_poisson
      id: 4
      report_type: FLEETWIDE_OCCURRENCE_COUNTS
      privacy_level: LOW_PRIVACY
      use_poisson_mechanism_for_privacy: true
      # Max is 1 error per second.
      min_value: 0
      max_value: 3600
      # Manually-specified parameters for epsilon = 10, delta = 1e(-6), sparsity = 9, population = 1e4
      poisson_mean: 0.00160138
      num_index_points: 10
      system_profile_field: [OS, ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL, REALM]
  meta_data:
    max_release_stage: DEBUG
    expiration_date: "2023/04/08"

#####################################################################
# error_occurred_components
#
# This is a fictional metric used for the development of Cobalt 1.1.
# We log each time an error occurs, noting the type of error and the
# component that it occurred on.
#####################################################################
- id: 22
  metric_name: error_occurred_components
  metric_type: STRING
  metric_semantics: [USAGE_COUNTING]
  metric_dimensions:
    - dimension: status
      event_codes:
        0: OK
        1: InvalidArgument
        2: FailedPrecondition
        9: Unknown
      max_event_code: 9
  string_candidate_file: fuchsia/test_app2/application_names.txt
  reports:
    #####################################################################
    # error_counts
    #
    # This is a fictional report used for the development of Cobalt 1.1.
    # For each day, we count the total number of occurrences over the
    # fleet of each error type for each component.
    #####################################################################
    - report_name: error_counts
      id: 1
      report_type: STRING_COUNTS
      privacy_level: NO_ADDED_PRIVACY
      string_buffer_max: 30
      system_profile_field: [OS, ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL, REALM]
    - report_name: error_counts_privacy
      id: 2
      report_type: STRING_COUNTS
      privacy_level: LOW_PRIVACY
      string_buffer_max: 30
      max_count: 10
      system_profile_field: [OS, ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL, REALM]
    - report_name: unique_error
      id: 3
      report_type: UNIQUE_DEVICE_STRING_COUNTS
      system_profile_selection: SELECT_LAST
      privacy_level: NO_ADDED_PRIVACY
      string_buffer_max: 30
      local_aggregation_period: WINDOW_7_DAYS
      system_profile_field: [OS, ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL, REALM]
    - report_name: unique_error_with_privacy
      id: 4
      report_type: UNIQUE_DEVICE_STRING_COUNTS
      system_profile_selection: SELECT_LAST
      privacy_level: LOW_PRIVACY
      string_buffer_max: 30
      local_aggregation_period: WINDOW_7_DAYS
      system_profile_field: [OS, ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL, REALM]
  meta_data:
    max_release_stage: DEBUG
    expiration_date: "2023/04/08"

#####################################################################
# file_system_cache_misses_new
#
# This is a fictional metric used for the development of Cobalt 1.1.
# We log how many times a cache miss occurs, noting the type of FS.
#####################################################################
- id: 13
  metric_name: file_system_cache_misses_new
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING]
  metric_dimensions:
    - dimension: encryption_state
      event_codes:
        0: "NotEncrypted"
        1: "Encrypted"
    - dimension: file_system_type
      event_codes:
        0: "blobfs"
        1: "minfs"
        2: "thinfs"
  reports:
    #####################################################################
    # file_system_cache_miss_counts
    #
    # This is a fictional report used for the development of Cobalt 1.1.
    # For each day and each registered file system type, we report the
    # total number of FS cache misses across the fleet. The FS types are
    # registered as part of the report definition.
    #####################################################################
    - report_name: file_system_cache_miss_counts
      id: 1
      report_type: FLEETWIDE_OCCURRENCE_COUNTS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME]
    - report_name: file_system_cache_miss_counts_privacy
      id: 4
      report_type: FLEETWIDE_OCCURRENCE_COUNTS
      privacy_level: LOW_PRIVACY
      # Max is one per millisecond.
      min_value: 0
      max_value: 3600000
      system_profile_field: [BOARD_NAME, PRODUCT_NAME]
    #####################################################################
    # file_system_cache_miss_histograms
    #
    # This is a fictional report used for the development of Cobalt 1.1.
    # For each day and each registered file system type, we report a
    # histogram of the number of FS cache misses per log.
    #####################################################################
    - report_name: file_system_cache_miss_histograms
      id: 2
      report_type: HOURLY_VALUE_HISTOGRAMS
      system_profile_selection: SELECT_LAST
      privacy_level: NO_ADDED_PRIVACY
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 10
          initial_step: 1
          step_multiplier: 2
      system_profile_field: [OS, ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL, REALM]
    - report_name: file_system_cache_miss_histograms_privacy
      id: 5
      report_type: HOURLY_VALUE_HISTOGRAMS
      system_profile_selection: SELECT_LAST
      privacy_level: LOW_PRIVACY
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 10
          initial_step: 1
          step_multiplier: 2
      system_profile_field: [OS, ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL, REALM]
    #####################################################################
    # file_system_cache_miss_stats
    #
    # This is a fictional report used for the development of Cobalt 1.1.
    # For each day and each registered file system type, we report
    # stats on the number of FS cache misses per log.
    #####################################################################
    - report_name: file_system_cache_miss_stats
      id: 3
      report_type: HOURLY_VALUE_NUMERIC_STATS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [OS, ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL, REALM]
    - report_name: file_system_cache_miss_stats_privacy
      id: 6
      report_type: HOURLY_VALUE_NUMERIC_STATS
      privacy_level: LOW_PRIVACY
      # Max is one per millisecond.
      min_value: 0
      max_value: 3600000
      system_profile_field: [OS, ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL, REALM]
  meta_data:
    max_release_stage: DEBUG
    expiration_date: "2023/04/08"

#####################################################################
# features_active_new
#
# This is a fictional metric used for the development of Cobalt 1.1.
# Each time we use one of our skills, we log the skill type.
#####################################################################
- id: 14
  metric_name: features_active_new
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING]
  metric_dimensions:
    - dimension: skill
      event_codes:
        0: Unset
        1: TimeTravel
        2: InvisibilityCloak
        3: HealingPotion
        9: Unknown
      max_event_code: 9
  reports:
    #####################################################################
    # features_active_unique_devices
    #
    # This is a fictional report used for the development of Cobalt 1.1.
    # We report the number of unique devices on which each skill was
    # used during rolling windows of 1 and 7 days, using local
    # differential privacy.
    #####################################################################
    - report_name: features_active_unique_devices
      id: 1
      report_type: UNIQUE_DEVICE_COUNTS
      system_profile_selection: SELECT_LAST
      privacy_level: NO_ADDED_PRIVACY
      local_aggregation_period: WINDOW_7_DAYS
      local_aggregation_procedure: AT_LEAST_ONCE
      system_profile_field: [OS, ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL, REALM]
      reporting_threshold: 1
    - report_name: features_active_unique_devices_privacy
      id: 2
      report_type: UNIQUE_DEVICE_COUNTS
      system_profile_selection: SELECT_LAST
      privacy_level: LOW_PRIVACY
      local_aggregation_period: WINDOW_7_DAYS
      local_aggregation_procedure: AT_LEAST_ONCE
      system_profile_field: [OS, ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL, REALM]
  meta_data:
    max_release_stage: DEBUG
    expiration_date: "2023/04/08"

#####################################################################
# update_duration_new
#
# This is a fictional metric used for the development of Cobalt 1.1.
# We log how long it takes (in microseconds) to complete specific
# steps in updating a device.
#####################################################################
- id: 15
  metric_name: update_duration_new
  metric_type: INTEGER
  metric_semantics: [LATENCY]
  metric_units: MICROSECONDS
  metric_dimensions:
    - dimension: error_name
      event_codes:
        0: Success
        1: TimedOut
        2: Cancelled
        3: Failed
    - dimension: update_duration_stage
      event_codes:
        0: DownloadPayload
        1: VerifyPayload
        2: ApplyPayload
        3: VerifyUpdate
  reports:
    #####################################################################
    # update_duration_timing_stats
    #
    # This is a fictional report used for the development of Cobalt 1.1.
    # For each registered update stage and each error code, we report
    # statistics of the duration of that update stage across the fleet.
    #####################################################################
    - report_name: update_duration_timing_stats
      id: 3
      report_type: FLEETWIDE_MEANS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME]
    - report_name: update_duration_timing_stats_privacy
      id: 4
      report_type: FLEETWIDE_MEANS
      privacy_level: LOW_PRIVACY
      # Max is one update per second.
      max_count: 3600
      min_value: -1000
      # Max is one hour in milliseconds.
      max_value: 3600000
      system_profile_field: [BOARD_NAME, PRODUCT_NAME]
    #####################################################################
    # update_duration_device_histograms
    #
    # This is a fictional report used for the development of Cobalt 1.1.
    # For each registered update stage and each error code, we report
    # a histogram of the mean duration of that update stage for each day
    # on each device.
    #####################################################################
    - report_name: update_duration_device_histograms
      id: 5
      report_type: UNIQUE_DEVICE_HISTOGRAMS
      system_profile_selection: SELECT_LAST
      local_aggregation_procedure: MEAN
      local_aggregation_period: WINDOW_1_DAY
      privacy_level: NO_ADDED_PRIVACY
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 10
          initial_step: 1
          step_multiplier: 2
      system_profile_field: [BOARD_NAME, PRODUCT_NAME]
    - report_name: update_duration_device_histograms_privacy
      id: 6
      report_type: UNIQUE_DEVICE_HISTOGRAMS
      system_profile_selection: SELECT_LAST
      local_aggregation_procedure: MEAN
      local_aggregation_period: WINDOW_1_DAY
      privacy_level: LOW_PRIVACY
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 10
          initial_step: 1
          step_multiplier: 2
      system_profile_field: [BOARD_NAME, PRODUCT_NAME]

  # Deleted report IDs that must not be reused.
  deleted_report_ids: [1,2]

  meta_data:
    max_release_stage: DEBUG
    expiration_date: "2023/04/08"

#####################################################################
# game_frame_rate_new
#
# This is a fictional metric used for the development of Cobalt 1.1.
# We log the frame rate of a game during different player actions
# in different environments.
#####################################################################
- id: 16
  metric_name: game_frame_rate_new
  metric_type: INTEGER
  metric_semantics: [FRAME_RENDERING]
  metric_units_other: "FPS"
  metric_dimensions:
    - dimension: player_action
      event_codes:
        0: Standing
        1: Walking
        2: Running
    - dimension: game_environment
      event_codes:
        0: Forest
        1: Beach
        2: Dungeon
        3: City
  reports:
    #####################################################################
    # game_frame_rate_histograms
    #
    # This is a fictional report used for the development of Cobalt 1.1.
    # For each registered game environment and each player action, we
    # report a histogram of the frame rate of the game across all logged
    # events for this metric in the fleet.
    #
    # The first histogram bucket has a width of 1000fps, and the width
    # doubles with each subsequent bucket.
    #####################################################################
    - report_name: game_frame_rate_histograms
      id: 1
      report_type: FLEETWIDE_HISTOGRAMS
      privacy_level: NO_ADDED_PRIVACY
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 10
          initial_step: 1000
          step_multiplier: 2
      system_profile_field: [OS, ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL, REALM]
    - report_name: game_frame_rate_histograms_privacy
      id: 4
      report_type: FLEETWIDE_HISTOGRAMS
      privacy_level: LOW_PRIVACY
      # Max is one logged value per second.
      max_count: 3600
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 5
          initial_step: 1000
          step_multiplier: 4
      system_profile_field: [OS, ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL, REALM]
    #####################################################################
    # game_frame_rate_histograms_linear_constant_width
    #
    # This is a fictional report used for the development of Cobalt 1.1.
    # For each registered game environment and each player action, we
    # report a histogram of the frame rate of the game across all logged
    # events for this metric in the fleet.
    #
    # All histogram buckets have the same width.
    #####################################################################
    - report_name: game_frame_rate_histograms_linear_constant_width
      id: 2
      report_type: FLEETWIDE_HISTOGRAMS
      privacy_level: NO_ADDED_PRIVACY
      int_buckets:
        linear:
          floor: 0
          num_buckets: 120
          step_size: 1000
      system_profile_field: [BOARD_NAME, PRODUCT_NAME]
    - report_name: game_frame_rate_histograms_linear_constant_width_privacy
      id: 5
      report_type: FLEETWIDE_HISTOGRAMS
      privacy_level: LOW_PRIVACY
      # Max is one logged value per second.
      max_count: 3600
      int_buckets:
        linear:
          floor: 0
          num_buckets: 8
          step_size: 15000
      system_profile_field: [BOARD_NAME, PRODUCT_NAME]
    #####################################################################
    # game_frame_rate_stats
    #
    # This is a fictional report used for the development of Cobalt 1.1.
    # For each registered game environment and each player action, we
    # report statistics of frame rate across all logged events for
    # this metric in the fleet.
    #####################################################################
    - report_name: game_frame_rate_stats
      id: 3
      report_type: FLEETWIDE_MEANS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [OS, ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL, REALM]
    - report_name: game_frame_rate_stats_privacy
      id: 6
      report_type: FLEETWIDE_MEANS
      privacy_level: LOW_PRIVACY
      # Max is one logged value per second.
      max_count: 3600
      min_value: 0
      # Max is exponential bucket max of 1023000, times 3600.
      max_value: 3682800000
      system_profile_field: [OS, ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL, REALM]
  meta_data:
    max_release_stage: DEBUG
    expiration_date: "2023/04/08"

#####################################################################
# application_memory_new
#
# This is a fictional metric used for the development of Cobalt 1.1.
# We log the memory usage of different applications running on the
# system.
#####################################################################
- id: 17
  metric_name: application_memory_new
  metric_type: INTEGER
  metric_semantics: [MEMORY_USAGE]
  metric_units: BYTES
  metric_dimensions:
    - dimension: memory_type
      event_codes:
        0: RSS
        1: VSZ
        2: PSS
    - dimension: application_name
      event_codes: &application_names
        0: audio_core
        1: chromium
        2: logger
        3: netstack
        4: scheduler
        5: sysmng
        6: view_manager
  reports:
    #####################################################################
    # application_memory_histograms
    #
    # This is a fictional report used for the development of Cobalt 1.1.
    # For each registered application and each type of memory, we
    # report a histogram of memory usage across all logged events for
    # this metric in the fleet.
    #
    # The first histogram bucket has a width of 1000 bytes, and the width
    # doubles with each subsequent bucket.
    #####################################################################
    - report_name: application_memory_histograms
      id: 1
      report_type: FLEETWIDE_HISTOGRAMS
      privacy_level: NO_ADDED_PRIVACY
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 10
          initial_step: 1000
          step_multiplier: 2
      system_profile_field: [BOARD_NAME, PRODUCT_NAME]
    - report_name: application_memory_histograms_privacy
      id: 4
      report_type: FLEETWIDE_HISTOGRAMS
      privacy_level: LOW_PRIVACY
      # Max is one logged value per second.
      max_count: 3600
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 4
          initial_step: 1000
          step_multiplier: 4
      system_profile_field: [BOARD_NAME, PRODUCT_NAME]
    #####################################################################
    # application_memory_histograms_linear_constant_width
    #
    # This is a fictional report used for the development of Cobalt 1.1.
    # For each registered application and each type of memory, we
    # report a histogram of memory usage across all logged events for
    # this metric in the fleet.
    #
    # All histogram buckets have the same width.
    #####################################################################
    - report_name: application_memory_histograms_linear_constant_width
      id: 2
      report_type: FLEETWIDE_HISTOGRAMS
      privacy_level: NO_ADDED_PRIVACY
      int_buckets:
        linear:
          floor: 0
          num_buckets: 50
          step_size: 5000
      system_profile_field: [BOARD_NAME, PRODUCT_NAME]
    - report_name: application_memory_histograms_linear_constant_width_privacy
      id: 5
      report_type: FLEETWIDE_HISTOGRAMS
      privacy_level: LOW_PRIVACY
      # Max is one logged value per second.
      max_count: 3600
      int_buckets:
        linear:
          floor: 0
          num_buckets: 4
          step_size: 50000
      system_profile_field: [BOARD_NAME, PRODUCT_NAME]
    #####################################################################
    # application_memory_stats
    #
    # This is a fictional report used for the development of Cobalt 1.1.
    # For each registered application and each player action, we
    # report statistics of the memory usage across all logged events for
    # this metric in the fleet.
    #####################################################################
    - report_name: application_memory_stats
      id: 3
      report_type: FLEETWIDE_MEANS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME]
    - report_name: application_memory_stats_privacy
      id: 6
      report_type: FLEETWIDE_MEANS
      privacy_level: LOW_PRIVACY
      # Max is one logged value per second.
      max_count: 3600
      min_value: 0
      # Max is exponential bucket max of 1023000, times 3600.
      max_value: 3682800000
      system_profile_field: [BOARD_NAME, PRODUCT_NAME]
  meta_data:
    max_release_stage: DEBUG
    expiration_date: "2023/04/08"

#####################################################################
# power_usage_new
#
# This is a fictional metric used for the development of Cobalt 1.1.
# Each hour, we log the foreground/background state of each
# application and a histogram bucket index corresponding to that
# application's power usage in mAh over the last hour.
#####################################################################
- id: 18
  metric_name: power_usage_new
  metric_type: INTEGER_HISTOGRAM
  metric_semantics: [METRIC_SEMANTICS_UNSPECIFIED]
  metric_units_other: "mAh"
  metric_dimensions:
    - dimension: application_state
      event_codes:
        0: Foreground
        1: Background
    - dimension: application_name
      event_codes: *application_names
  int_buckets:
    linear:
      floor: 0
      num_buckets: 50
      step_size: 2
  reports:
    #####################################################################
    # power_usage_histograms
    #
    # This is a fictional report used for the development of Cobalt 1.1.
    # For each registered application name and each application state,
    # we report a histogram of the hourly power usage over all logged
    # events in the fleet.
    #####################################################################
    - report_name: power_usage_histograms
      id: 1
      report_type: FLEETWIDE_HISTOGRAMS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME]
    - report_name: power_usage_histograms_privacy
      id: 2
      report_type: FLEETWIDE_HISTOGRAMS
      privacy_level: LOW_PRIVACY
      max_count: 3600
      event_vector_buffer_max: 2
      system_profile_field: [BOARD_NAME, PRODUCT_NAME]
  meta_data:
    max_release_stage: DEBUG
    expiration_date: "2023/04/08"

#####################################################################
# bandwidth_usage_new
#
# This is a fictional metric used for the development of Cobalt 1.1.
# Each hour, we log the foreground/background state of each
# application and a histogram bucket index corresponding to that
# application's bandwidth usage in KB over the last hour.
#####################################################################
- id: 19
  metric_name: bandwidth_usage_new
  metric_type: INTEGER_HISTOGRAM
  metric_semantics: [NETWORK_COMMUNICATION]
  metric_units_other: "KB/hour"
  metric_dimensions:
    - dimension: application_state
      event_codes:
        0: Foreground
        1: Background
    - dimension: application_name
      event_codes: *application_names
  int_buckets:
    exponential:
      floor: 0
      num_buckets: 20
      initial_step: 100
      step_multiplier: 2
  reports:
    #####################################################################
    # bandwidth_usage_histograms
    #
    # This is a fictional report used for the development of Cobalt 1.1.
    # For each registered application name and each application state,
    # we report a histogram of the hourly bandwidth usage over all logged
    # events in the fleet.
    #####################################################################
    - report_name: bandwidth_usage_histograms
      id: 1
      report_type: FLEETWIDE_HISTOGRAMS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME]
    - report_name: bandwidth_usage_histograms_privacy
      id: 2
      report_type: FLEETWIDE_HISTOGRAMS
      privacy_level: LOW_PRIVACY
      max_count: 3600
      event_vector_buffer_max: 5
      system_profile_field: [BOARD_NAME, PRODUCT_NAME]
  meta_data:
    max_release_stage: DEBUG
    expiration_date: "2023/04/08"

#####################################################################
# connection_attempts_new
#
# This is a fictional metric used for the development of Cobalt 1.1.
# We log the number of successful and failed connection attempts,
# for each host, once per minute.
#####################################################################
- id: 20
  metric_name: connection_attempts_new
  metric_type: OCCURRENCE
  metric_semantics: [NETWORK_COMMUNICATION]
  metric_dimensions:
    - dimension: status
      event_codes:
        0: Success
        1: Failure
    - dimension: host_name
      event_codes:
        0: HostA
        1: HostB
        2: HostC
  reports:
    #####################################################################
    # connection_attempts_per_device_count
    #
    # This is a fictional report used for the development of Cobalt 1.1.
    # We use on-device aggregation to collect the per-device total
    # numbers of connection attempts over 1-day and 7-day rolling
    # windows. We then report aggregate statistics of those per-device
    # counts over the fleet, broken down by host name and status code.
    #####################################################################
    - report_name: connection_attempts_per_device_count
      id: 1
      report_type: UNIQUE_DEVICE_NUMERIC_STATS
      privacy_level: NO_ADDED_PRIVACY
      local_aggregation_period: WINDOW_7_DAYS
      system_profile_field: [OS, ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL, REALM]
    - report_name: connection_attempts_per_device_count_privacy
      id: 2
      report_type: UNIQUE_DEVICE_NUMERIC_STATS
      privacy_level: LOW_PRIVACY
      # Up to 10000 attempts per day.
      min_value: 0
      max_value: 70000
      local_aggregation_period: WINDOW_7_DAYS
      system_profile_field: [OS, ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL, REALM]
  meta_data:
    max_release_stage: DEBUG
    expiration_date: "2023/04/08"

#####################################################################
# streaming_time_new
#
# This is a fictional metric used for the development of Cobalt.
# At the end of each media streaming event, we log the stream's
# duration.
#####################################################################
- id: 21
  metric_name: streaming_time_new
  metric_type: INTEGER
  metric_semantics: [LATENCY]
  metric_units: SECONDS
  metric_dimensions:
    - dimension: type
      event_codes:
        0: StreamType0
        1: StreamType1
        2: StreamType2
    - dimension: module_name
      event_codes:
        0: ModuleA
        1: ModuleB
        2: ModuleC
  reports:
    #####################################################################
    # streaming_time_per_device_total
    #
    # This is a fictional report used for the development of Cobalt.
    # We use on-device aggregation to collect the per-device total
    # streaming time over a 7-day rolling windows. We then
    # report aggregate statistics of those per-device totals over the
    # fleet, broken down by module name and stream type.
    #####################################################################
    - report_name: streaming_time_per_device_total
      id: 1
      report_type: UNIQUE_DEVICE_NUMERIC_STATS
      privacy_level: NO_ADDED_PRIVACY
      local_aggregation_period: WINDOW_7_DAYS
      local_aggregation_procedure: SUM_PROCEDURE
      system_profile_field: [BOARD_NAME, PRODUCT_NAME]
    - report_name: streaming_time_per_device_total_privacy
      id: 2
      report_type: UNIQUE_DEVICE_NUMERIC_STATS
      privacy_level: LOW_PRIVACY
      # Max is 7 days in seconds.
      min_value: 0
      max_value: 604800
      local_aggregation_period: WINDOW_7_DAYS
      local_aggregation_procedure: SUM_PROCEDURE
      system_profile_field: [BOARD_NAME, PRODUCT_NAME]
  meta_data:
    max_release_stage: DEBUG
    expiration_date: "2023/04/08"

# Deleted metric IDs that must not be reused.
deleted_metric_ids: [1,2,3,4,5,6,7,8,9,10,11]
