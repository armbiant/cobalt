# Copyright 2019 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

metric_definitions:

#####################################################################
# crash
#
# Count of times the crash reporter handles crash reports in various
# states.
# We send the event when performing actions on a crash report.
#####################################################################
- id: 102
  metric_name: crash_migrated
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING]
  metric_dimensions:
    - dimension: state
      event_codes:
        0: Unknown
        1: Filed
        2: Uploaded
        3: Archived
        4: GarbageCollected
        5: Dropped
        6: UploadThrottled
        7: OnDeviceQuotaReached
        8: Deleted
        9: UploadTimedOut
      max_event_code: 32
  reports:
    - report_name: crash_state_event_counts
      id: 1
      report_type: FLEETWIDE_OCCURRENCE_COUNTS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/10/01"

#####################################################################
# crash_upload_attempts
#
# We log the current upload attempt number when:
# * we attempt to upload a crash report
# * we successfully upload a crash report
# * we delete a crash report that we attempted to upload at least
#   once
# * we garbage collect a crash report that we attempted to upload at
#   least once
# * we attempt to upload a report and are throttled by the server
# * we attempt to upload a report and the request times out
#
# Every time, we log the attempt number.
#####################################################################
- id: 103
  metric_name: crash_upload_attempts_migrated
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING]
  metric_dimensions:
    - dimension: state
      event_codes:
        0: Unknown
        1: UploadAttempt
        2: Uploaded
        3: Deleted
        4: GarbageCollected
        5: UploadThrottled
        6: UploadTimedOut
      max_event_code: 16
  reports:
      #####################################################################
      # histogram
      #
      # Determine the distribution of upload attempts.
      #####################################################################
    - report_name: histogram
      id: 1
      report_type: HOURLY_VALUE_HISTOGRAMS
      system_profile_selection: SELECT_LAST
      privacy_level: NO_ADDED_PRIVACY
      int_buckets:
        linear:
          floor: 1
          num_buckets: 64
          step_size: 1
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/10/01"

#####################################################################
# feedback_data_collection_timeout
#
# Record the number of times each data collection function times out
# during feedback data collection.
# We send an event each time collection of some data times out.
#####################################################################
- id: 105
  metric_name: feedback_data_collection_timeout_migrated
  metric_type: OCCURRENCE
  metric_semantics: [USAGE_COUNTING]
  metric_dimensions:
    - dimension: data
      event_codes:
        0: Unknown
        1: SystemLog
        2: KernelLog
        3: Screenshot
        4: Inspect
        5: Channel
        6: ProductInfo
        7: BoardInfo
        8: LastRebootInfo
      max_event_code: 32
  reports:
      #####################################################################
      # counts
      #
      # Count the number of times each data collection funtion times out.
      #####################################################################
    - report_name: counts
      id: 1
      report_type: FLEETWIDE_OCCURRENCE_COUNTS
      privacy_level: NO_ADDED_PRIVACY
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/10/01"

#####################################################################
# snapshot_generation_duration_usecs
#
# Whenever a snapshot is generated, we log the elapsed time in
# microseconds from when the snapshot collection is started to when
# the snapshot is ready to be sent.
#
# Snapshots are primarily requested in response to user feedback and
# program crashes, so we expect this metric to be recorded at minute
# level frequency, with the potential for faster bursts in the
# circumstance where programs crash repeatedly.
# #####################################################################
- id: 106
  metric_name: snapshot_generation_duration_usecs_migrated
  metric_type: INTEGER
  metric_units: MICROSECONDS
  metric_semantics: [LATENCY]
  metric_dimensions:
    - dimension: flow
      event_codes:
        0: Unknown
        1: Success
        2: Failure
  reports:
      #####################################################################
      # histogram
      #
      # Determine the distribution of snapshot generation durations
      # between 0 and 120 seconds, using 1 second wide buckets.
      #####################################################################
    - report_name: histogram
      id: 1
      report_type: FLEETWIDE_HISTOGRAMS
      privacy_level: NO_ADDED_PRIVACY
      int_buckets:
        linear:
          floor: 0
          num_buckets: 120
          step_size: 1000000 # 1 second
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/10/01"

#######################################################################
# reboot_reason_persist_duration_usecs
#
# Whenever last_reboot.cmx receives a notification that the system is
# going to shutdown we log the elapsed time in microseconds that it
# takes to persist the reboot reason to disk. This metric is collected
# once each time a device shuts down.
#######################################################################
- id: 107
  metric_name: reboot_reason_persist_duration_usecs_migrated
  metric_type: INTEGER
  metric_units: MICROSECONDS
  metric_semantics: [LATENCY]
  metric_dimensions:
    - dimension: write_result
      event_codes:
        0: Success
        1: Failure
  reports:
      #####################################################################
      # histogram
      #
      # Determine the distribution of how long it takes to persist the
      # reboot reason between 0 and 5 seconds, using 0.1 second wide
      # bucket.
      #####################################################################
    - report_name: histogram
      id: 1
      report_type: FLEETWIDE_HISTOGRAMS
      privacy_level: NO_ADDED_PRIVACY
      int_buckets:
        linear:
          floor: 0
          num_buckets: 50
          step_size: 100000 # .1 second
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/10/01"

#####################################################################
# last_reboot_uptime
#
# The uptime of the last boot in microseconds and what caused the
# device to reboot.
# We send the event once after a reboot.
#####################################################################
- id: 108
  metric_name: last_reboot_uptime_migrated
  metric_type: INTEGER
  metric_units: MICROSECONDS
  metric_semantics: [LATENCY]
  metric_dimensions:
    - dimension: reason
      event_codes:
        0: Unknown
        1: GenericGraceful
        2: GenericUngraceful
        3: Cold
        4: BriefPowerLoss
        5: Brownout
        6: KernelPanic
        7: SystemOutOfMemory
        8: HardwareWatchdogTimeout
        9: SoftwareWatchdogTimeout
        10: UserRequest
        11: SystemUpdate
        12: HighTemperature
        13: SessionFailure
        14: SysmgrFailure
        15: FactoryDataReset
        16: CriticalComponentFailure
        17: RetrySystemUpdate
        18: ZbiSwap
        19: RootJobTermination
  reports:
      #####################################################################
      # histogram
      #
      # Distribution of uptimes for each type of reboot reason in
      # microseconds.
      #####################################################################
    - report_name: histogram
      id: 1
      report_type: FLEETWIDE_HISTOGRAMS
      privacy_level: NO_ADDED_PRIVACY
      int_buckets:
        exponential:
          floor: 0
          num_buckets: 25
          initial_step: 60000000 # 1 minute
          step_multiplier: 2
      system_profile_field: [OS, ARCH, BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL, REALM]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/10/01"

#####################################################################
# previous_boot_log_compression_ratio
#
# The truncated compression_ratio * 100 for the previous boot logs,
# e.g., 100 = no compression gain nor loss, 200 = 2x compression.
# We send this metric once after a reboot.
#####################################################################
- id: 109
  metric_name: previous_boot_log_compression_ratio_migrated
  metric_type: INTEGER
  metric_units_other: "Ratio * 100"
  metric_semantics: [DATA_SIZE, MEMORY_USAGE]
  metric_dimensions:
    # Represents how the data was compressed, e.g, using lz4.
    - dimension: encoding_version
      event_codes:
        0: Unknown
        1: V_01
        2: V_02
        3: V_03
        4: V_04
        5: V_05
  reports:
      #####################################################################
      # stats
      #
      # Statistics for the metric value (mean, median, percentiles, etc.).
      #####################################################################
    - report_name: stats_10
      id: 1
      report_type: HOURLY_VALUE_NUMERIC_STATS
      privacy_level: NO_ADDED_PRIVACY
      local_aggregation_procedure: PERCENTILE_N
      local_aggregation_procedure_percentile_n: 10
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
    - report_name: stats_25
      id: 2
      report_type: HOURLY_VALUE_NUMERIC_STATS
      privacy_level: NO_ADDED_PRIVACY
      local_aggregation_procedure: PERCENTILE_N
      local_aggregation_procedure_percentile_n: 25
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
    - report_name: stats_75
      id: 3
      report_type: HOURLY_VALUE_NUMERIC_STATS
      privacy_level: NO_ADDED_PRIVACY
      local_aggregation_procedure: PERCENTILE_N
      local_aggregation_procedure_percentile_n: 75
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
    - report_name: stats_90
      id: 4
      report_type: HOURLY_VALUE_NUMERIC_STATS
      privacy_level: NO_ADDED_PRIVACY
      local_aggregation_procedure: PERCENTILE_N
      local_aggregation_procedure_percentile_n: 90
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/10/01"


#####################################################################
# snapshot_size
#
# A Fuchsia snapshot is a bundle of files representing the current
# state of the device, e.g., logs, Inspect data that we attach to
# feedback reports. This metric tracks the file size in bytes of that
# bundle and we log the metric every time we generate a snapshot.
#####################################################################
- id: 110
  metric_name: snapshot_size_migrated
  metric_type: INTEGER
  metric_units: BYTES
  metric_semantics: [DATA_SIZE, MEMORY_USAGE]
  metric_dimensions:
    # Represents how the snapshot was generated, e.g., using ZIP.
    - dimension: version
      event_codes:
        0: Unknown
        1: V_01
        2: V_02
        3: V_03
  reports:
      #####################################################################
      # stats
      #
      # Statistics for the metric value (mean, median, percentiles, etc.).
      #####################################################################
    - report_name: stats_10
      id: 1
      report_type: HOURLY_VALUE_NUMERIC_STATS
      privacy_level: NO_ADDED_PRIVACY
      local_aggregation_procedure: PERCENTILE_N
      local_aggregation_procedure_percentile_n: 10
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
    - report_name: stats_25
      id: 2
      report_type: HOURLY_VALUE_NUMERIC_STATS
      privacy_level: NO_ADDED_PRIVACY
      local_aggregation_procedure: PERCENTILE_N
      local_aggregation_procedure_percentile_n: 25
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
    - report_name: stats_75
      id: 3
      report_type: HOURLY_VALUE_NUMERIC_STATS
      privacy_level: NO_ADDED_PRIVACY
      local_aggregation_procedure: PERCENTILE_N
      local_aggregation_procedure_percentile_n: 75
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
    - report_name: stats_90
      id: 4
      report_type: HOURLY_VALUE_NUMERIC_STATS
      privacy_level: NO_ADDED_PRIVACY
      local_aggregation_procedure: PERCENTILE_N
      local_aggregation_procedure_percentile_n: 90
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/10/01"

#####################################################################
# max_input_inspect_budget
#
# The max_input_inspect_budget is a dynamic value that is calculated
# every time we create a new snapshot. This value is sent to inspect
# to limit the amount of inspect data that is included in the next
# snapshot to produced a compressed snapshot size lower than a certain
# threshold. This metric combined with the snapshot size metric can
# give us an early warning sign if we might be trimming too much data.
#
# Snapshots are primarily requested in response to user feedback and
# program crashes, so we expect this metric to be recorded at minute
# level frequency, with the potential for faster bursts in the
# circumstance where programs crash repeatedly.
#####################################################################
- id: 111
  metric_name: max_input_inspect_budget_migrated
  metric_type: INTEGER
  metric_units: BYTES
  metric_semantics: [DATA_SIZE, MEMORY_USAGE]
  reports:
    - report_name: stats_min
      id: 1
      report_type: HOURLY_VALUE_NUMERIC_STATS
      privacy_level: NO_ADDED_PRIVACY
      local_aggregation_procedure: MIN_PROCEDURE
      system_profile_field: [BOARD_NAME, PRODUCT_NAME, SYSTEM_VERSION, CHANNEL]
  meta_data:
    max_release_stage: GA
    expiration_date: "2023/10/01"

# Deleted metric IDs that must not be reused.
deleted_metric_ids: [1,2,3,4,5,6,7,8,9,10,11]
